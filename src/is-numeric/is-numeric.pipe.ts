import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class IsNumericPipe implements PipeTransform {
  transform(value: any) {
    const isNumeric = /^\d+$/.test(value);
    if (!isNumeric) throw new BadRequestException('Value is not numeric');
    return value;
  }
}
