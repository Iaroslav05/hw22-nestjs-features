import { Injectable } from '@nestjs/common';

@Injectable()
export class HashService {
  encodeString(str: string): string {
    return Buffer.from(str).toString('base64');
  }
  decodeString(str: string): string {
    return Buffer.from(str, 'base64').toString('utf8');
  }
}
