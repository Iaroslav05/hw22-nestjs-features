import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateRecordDto } from './dto/create-record.dto';
import { UpdateRecordDto } from './dto/update-record.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Record } from './entities/record.entity';
import { Repository } from 'typeorm';
import { HashService } from '../common/hash.service';

@Injectable()
export class RecordService {
  constructor(
    @InjectRepository(Record)
    private readonly recordRepository: Repository<Record>,
    private readonly hashService: HashService,
  ) {}
  async create(createRecordDto: CreateRecordDto) {
    const isExist = await this.recordRepository.findOne({
      where: { name: createRecordDto.name },
    });
    if (isExist) throw new BadRequestException('Record already exists');
    return await this.recordRepository.save({
      name: createRecordDto.name,
      content: this.hashService.encodeString(createRecordDto.content),
    });
  }

  async findAll(): Promise<Record[]> {
    return await this.recordRepository.find();
  }

  async findOneEncoded(id: number) {
    const isExist = await this.recordRepository.findOne({ where: { id } });

    if (!isExist) throw new NotFoundException('Record not found');

    return isExist;
  }

  async findOneDecoded(id: number) {
    const isExist = await this.recordRepository.findOne({ where: { id } });
    if (!isExist) throw new NotFoundException('Record not found');

    isExist.content = this.hashService.decodeString(isExist.content);
    return isExist;
  }

  async update(id: number, updateRecordDto: UpdateRecordDto) {
    const isExist = await this.recordRepository.findOne({ where: { id } });

    if (!isExist) throw new NotFoundException('Record not found');

    return await this.recordRepository.update(id, updateRecordDto);
  }

  async remove(id: number) {
    const isExist = await this.recordRepository.findOne({ where: { id } });

    if (!isExist) throw new NotFoundException('Record not found');

    return await this.recordRepository.delete(id);
  }
}
