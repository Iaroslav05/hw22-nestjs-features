import { Injectable } from '@nestjs/common';
import * as fsPromises from 'fs/promises';
import * as path from 'path';

@Injectable()
export class LoggerService {
  log(name: string, data: any) {
    const nameFileLog = new Date()
      .toISOString()
      .slice(0, 10)
      .split('-')
      .reverse()
      .join('-');

    const patchLodsFile = path.join(__dirname, `../../logs/${nameFileLog}.log`);
    fsPromises.appendFile(
      patchLodsFile,
      `${name} ${JSON.stringify(data, null, 2)}` + '\n',
      'utf8',
    );
  }
}
