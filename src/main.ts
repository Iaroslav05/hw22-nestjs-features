import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AllErrorExceptionFilter } from './all-error-exception/all-error-exception.filter';
import { ForbiddenErrorExceptionFilter } from './forbidden-error-exception/forbidden-error-exception.filter';
import { LoggerInterceptor } from './logger/logger.interceptor';
import { LoggerService } from './logger/logger.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new AllErrorExceptionFilter());
  app.useGlobalFilters(new ForbiddenErrorExceptionFilter());
  app.setGlobalPrefix('api');
  app.enableCors();
  app.useGlobalInterceptors(new LoggerInterceptor(new LoggerService()));
  await app.listen(3000);
}
bootstrap();
